﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;


public class PicOutline : MonoBehaviour
{
    public List<DrowLine> Lines = new List<DrowLine>();
    public Transform Drawer;
     public float DrawSpeed; 
     public float GrowSpeed;
     public float MeshHeight;
    public int ActiveLine = -1;
    public GameObject SplineExample;
    public MeshRenderer Model;
    
    public GameObject OutlineOnPlane;
    GameObject OutlineInCenter;
    float ModelHeght = 10;
    bool isDrownPic = false;
 
    public delegate void PrintFrame(float height);
    public delegate void EndPrintEvent();
    public static event PrintFrame printOneFrame;
    public static event EndPrintEvent EndPrint;
    public static Vector3 StartPos;
    // bool PhaseFlag;

    
    void OnEnable(){
        DrowLine.onDrawn += DrawnOneLine;
    }
    void OnDisable(){
        DrowLine.onDrawn -= DrawnOneLine;
    }
    void Start(){
        // PhaseFlag = false;
      
        ActiveLine = -1;
        GameManager.MoveCam(0);
        CreateOutlines();
        // StartPos  = Lines[0].GetComponent<BGCcMath>().CalcPositionByDistance(0);
        StartPos  = Lines[0].GetComponent<BGCurve>().Points[0].PositionWorld;

          GameManager.GamePhase = 0.5f;
    }
    void Update(){
        if(GameManager.GamePhase == 0 &&  OutlineMatchCheck()){//когда сопоставил обводки
            GameManager.GamePhase =1;
            OutlineInCenter.SetActive(false);
            
            
        }
                    // Debug.Log(GameManager.GamePhase+" "+!PhaseFlag+"  "+  Input.GetMouseButtonDown(0));

        if(GameManager.GamePhase == 2 && Input.GetMouseButtonDown(0)){
            GameManager.GamePhase = 3;
            NextLine();
        }
    }

    public void DrawnOneLine(){
        NextLine();
        
    }
    public void ResetPicture(){
        foreach (var line in Lines)
        {
            line.ResetLine();
        }
    }
    void NextLine(){
        
        if(ActiveLine>=Lines.Count-1){
            // SpawnMeshes();
            
           GameManager.MoveCam(1);
            StartCoroutine(PrintModel()) ;
            // Drawer.gameObject.SetActive(false);
            GameManager.GamePhase = 4;
            return;
        }
        ActiveLine++;

        Lines[ActiveLine].cursorobj = Drawer;
        Lines[ActiveLine].DrawSpeed = DrawSpeed;
        Lines[ActiveLine].ToggleActiveLine(true);
    }

    

    IEnumerator PrintModel(){
        float height = 0;
        while (height<=MeshHeight){
            printOneFrame(height);
            Model.material.SetVector("_PlanePoint",new Vector4(0,height,0,0));
            height += Time.deltaTime*GrowSpeed;
            
            yield return new WaitForFixedUpdate();
        }
        EndPrint();
    }



    void CreateOutlines(){
        var CenterOfPrinter = new Vector3(transform.position.x,transform.position.y,6.2f);

        OutlineOnPlane = new GameObject();
        OutlineOnPlane.name  = "OutlineOnPlane";
        OutlineOnPlane.transform.parent = transform.parent;
        OutlineOnPlane.transform.position = CenterOfPrinter;

            CopyLines(OutlineOnPlane.transform,new Color(0.6f,0.6f,0.6f,0.6f));


        OutlineInCenter = new GameObject();
        OutlineInCenter.name  = "OutlineInCenter";
        OutlineInCenter.transform.position = CenterOfPrinter; 

        CopyLines(OutlineInCenter.transform,new Color(0.5f,0.5f,0.5f,0.4f));   
        //  OutlineInCenter.transform.position = new Vector3(0,0,18f);
            
            
        // return OutlineOnPlane;
    }
    
    void CopyLines(Transform parent, Color lineColor){
        foreach (var line in Lines)
        {
            var lp = line.transform.position;
            var lr = line.transform.rotation;
            var lineRend = Instantiate(line.GetComponent<LineRenderer>(), new Vector3(lp.x,lp.y-0.005f,lp.z) , lr,parent);
            lineRend.widthCurve = AnimationCurve.Linear(0,1,0,1);
            lineRend.material.color = lineColor;
         
            lineRend.GetComponent<DrowLine>().enabled = false;

            // lineRend.GetComponent<BGCurve>().enabled = false;
            // lineRend.sortingOrder = /
            // lineRend.material.renderQueue = line.GetComponent<LineRenderer>().material.renderQueue+10;
        }  
    }

     bool OutlineMatchCheck(){
        //   Debug.Log("check" + GameManager.GamePhase);
        var minmag = 0.05f;
        var mag = Mathf.Abs(OutlineOnPlane.transform.position.z-OutlineInCenter.transform.position.z);
        // Debug.Log(OutlineInCenter.transform.position.z+"  "+OutlineOnPlane.transform.position.z+"  mag="+mag+"  "+minmag);
        return mag <= minmag;
    }

    void OnDestroy(){
        Destroy(OutlineInCenter);
        Destroy(OutlineOnPlane);
    }

}
