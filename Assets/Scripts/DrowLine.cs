﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;

public class DrowLine : MonoBehaviour
{
    public bool ActiveDraw=false;
     public float DrawSpeed; //units per sec
     float Duration;
    public float Smooth = 0.1f;
    public Transform cursorobj;
    LineRenderer Line ;
    AnimationCurve Curve;
    // BGCcCursor Cursor;
    // BGCcCursorObjectTranslate CursorObjectScript;
    BGCcMath math;

    public delegate void DrawEvent();
    public delegate void DrawFrameEvent(Vector3 pos);
    public static event DrawEvent onDrawn;
    public static event DrawFrameEvent DrawFrame;

     float time;
    void Awake()
    {   
        math = GetComponent<BGCcMath>();
       Line = GetComponent<LineRenderer>();
    //    Cursor= GetComponent<BGCcCursor>();
    //    CursorObjectScript =  Cursor.GetComponent<BGCcCursorObjectTranslate>();
    //    Debug.Log(CursorObjectScript);
        // CursorObjectScript.enabled = false;
         
        ResetLine();

        ActiveDraw=false;
        // Cursor
    }

    public void ToggleActiveLine(bool active){
        ActiveDraw  = active;
        // Debug.Log(CursorObjectScript);
        //  CursorObjectScript.enabled = active;
        if(active){
        var len = math.GetDistance();
        Duration = len/DrawSpeed;
        // CursorObjectScript.ObjectToManipulate =  cursorobj;
        } 
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!ActiveDraw) return;


        if(Input.GetMouseButton(0)){
           Draw();
        //   cursorobj.transform.GetChild(0).localPosition  = Vector3.up*1.5f;
        }else{
            //  cursorobj.transform.GetChild(0).localPosition  = Vector3.up*3f;
        }
         
        // else Erase();
        // Debug.Log(CursorObjectScript.gameObject.name); 
        // Cursor.DistanceRatio = time;
        
    }


    Keyframe GetKey(int width){
        var i = width;
        var t = width == 0 ? time + Smooth/2 : time- Smooth/2;
        return new Keyframe(t, i);
    }

    void Draw(){
        if(time>=1+Smooth) {
            if(ActiveDraw){
                onDrawn();
                ToggleActiveLine(false);
            }
            return;
        }
        

       
        time+=Time.deltaTime/Duration;
        Curve.RemoveKey(0);
        Curve.RemoveKey(0);
        Curve.AddKey(GetKey(1));
        Curve.AddKey(GetKey(0));
        Line.widthCurve = Curve;  

        var curpos = transform.parent.TransformVector(math.CalcPositionByDistanceRatio(time)) ;
         DrawFrame(curpos);

    }
    void Erase(){
        if(time<=0){
            // Cursor.GetComponent<BGCcCursorObjectTranslate>().enabled =true;
            return;
        }
        time-=Time.deltaTime/Duration;
        Curve.RemoveKey(0);
        Curve.RemoveKey(0);
        Curve.AddKey(GetKey(0));
        Curve.AddKey(GetKey(1));
        Line.widthCurve = Curve;  
    }
    public void ResetLine(){
        Curve = new AnimationCurve(new Keyframe(0, 0),new Keyframe(Smooth, 0));
        Line.widthCurve = Curve;
    }

}
