﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;

public class GameManager : MonoBehaviour
{
    public int CurrentPic;
    public List<GameObject> Pictures = new List<GameObject>();
    public List<Transform> CamPos = new List<Transform>();
    static List<Transform> StaticCamPos = new List<Transform>();
    public Printer printer;
    public GameObject Picture;
    public static float GamePhase; 
    public GameObject nextBtn;
    
    // -1 - start Game,0.5 - picture ready, 0 - swipe outline, 1 - done outline, 2 - start draw, 3 - draw, 4 - end draw/start print, 5 - print, 6 - end print
    void Awake(){
        GameAnalytics.Initialize();
         StaticCamPos = CamPos;
         
        GamePhase = -1;
    }
    void Start()
    {
        CurrentPic = 0;
        SpawnPic();
        
        
    }

    void SpawnPic(){
         GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Start level");


        Picture = Instantiate(Pictures[CurrentPic],printer.Plane.GetChild(0).position + Vector3.up*0.62f,Quaternion.identity,printer.Plane.GetChild(0));
    }
     public void Restart(bool next){
         nextBtn.SetActive(false);
        printer.Plane.localPosition = new Vector3(printer.Plane.localPosition.x,printer.Plane.localPosition.y,0);
        if (next) IncPicNum();
        Destroy(Picture);
        SpawnPic();
        // printer.Restart();
    }

    void IncPicNum(){
        CurrentPic++;
        if (CurrentPic>=Pictures.Count){
            CurrentPic = 0;
        }
    }
     public void RestartPicTEMP(){
        SceneManager.LoadScene(0);

    }
    // public void NextPic(){
    //     Destroy(Picture);
    //     SpawnPic();
    // }
    public static void MoveCam(int pos){
        Camera.main.transform.position = StaticCamPos[pos].position;
        Camera.main.transform.rotation = StaticCamPos[pos].rotation;
    }
}
