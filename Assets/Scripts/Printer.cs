﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class Printer : MonoBehaviour
{
    public Transform Plane;
    public Transform Hotpod;
    public Transform Сarriage;
    public Transform Stand;
    public float СarriageHeight;
    public float СarriageSpeed;
    public float vfxTime;
    public ParticleSystem Puf;

    
    Vector3 RndPos;
    float MoveTime;
    Vector3 step;
    bool rndmove;
    Vector3 CurrentPos;
    Vector3 StartPos;
    GameObject PrinterCursor;
    float PrevScreenPosZ=-1000;
    float StartTime;
    Vector2 PlaneMoveBorder = new Vector2(12,-0.5f);
    
    float size = 13; //13*13*13 units
    void OnEnable(){
        // DrowLine.onDrawn += DrawnOneLine;
        DrowLine.DrawFrame += SetCursorPosOnly;
        PicOutline.printOneFrame += RndGrowPrint;
        PicOutline.EndPrint += EndPrint;
    }
    void OnDisable(){
        // DrowLine.onDrawn -= DrawnOneLine;
        DrowLine.DrawFrame -= SetCursorPosOnly;
        PicOutline.printOneFrame -= RndGrowPrint;
        PicOutline.EndPrint -= EndPrint;
    }

    // Update is called once per frame
    void Start()
    {
        PrinterCursor = new GameObject();
        PrinterCursor.transform.parent = Plane;
        PrinterCursor.name = "PrinterCursor"; 
        // Restart();
    }

    public void Restart(){
                rndmove = false;
        StartTime = -1;

        ToggleСarriage(false);
        // ToggleStand(false);
         ToggleHotpod(false);

        SetPlaneRndPos();
        GameManager.GamePhase = 0;
        
    }
    void Update(){
        if(GameManager.GamePhase == 0.5f){
        Restart();
        }

        if(GameManager.GamePhase == 0 ){
            if(Input.GetMouseButton(0)){
                if(StartTime ==-1) StartTime = Time.time;
                var mpos = Input.mousePosition;

                // mpos.z = -Camera.main.transform.position.z+Plane.position.z;
                // mpos= Camera.main.ScreenToWorldPoint(mpos);
                var mposZ = mpos.y;
                if (PrevScreenPosZ == -1000)  PrevScreenPosZ=mposZ;

                var delta = mposZ - PrevScreenPosZ;
                delta*= (PlaneMoveBorder.x-PlaneMoveBorder.y)/Screen.height;
                var planePosZ = Plane.position.z + delta;
                if(planePosZ>PlaneMoveBorder.x)planePosZ=PlaneMoveBorder.x;
                if(planePosZ<PlaneMoveBorder.y)planePosZ=PlaneMoveBorder.y;
                Plane.transform.position = new Vector3(Plane.transform.position.x,Plane.transform.position.y,planePosZ);
                Debug.Log(mposZ);
                PrevScreenPosZ=mposZ;
            }else PrevScreenPosZ = -1000;
            
                // Vector3 mousePos = Input.mousePosition;
    
    // var worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
    // Debug.Log(worldPosition);
        }
        if(GameManager.GamePhase == 1){
            if(Time.time - StartTime <= vfxTime)Puf.Play();
            
            ToggleСarriage(true);
            Сarriage.transform.position = PicOutline.StartPos;
            // ToggleStand(true);
            // Plane.transform.localPosition = new Vector3(Plane.transform.localPosition.x,Plane.transform.localPosition.y,0);
            GameManager.GamePhase = 2;
        }

    
        if(GameManager.GamePhase == 4){
            ToggleHotpod(true);
            Сarriage.transform.localPosition = Vector3.zero;
            GameManager.GamePhase = 5;
        }

    }

    public void SetPosition(Vector3 pos){
      

        var wpos = pos;
        // var wpos = new Vector3(-0.7f,3.7f,0.3f);
        
        var planedelta = transform.position.z+(Plane.position.z - wpos.z );
        var carriagedelta = -transform.position.x + wpos.x;
        var hotpoddelta = Plane.position.y + wpos.y +СarriageHeight;
      

        Plane.position = new Vector3(Plane.position.x,Plane.position.y,planedelta);
        Сarriage.position = new Vector3(carriagedelta,Сarriage.position.y,Сarriage.position.z);
        Hotpod.position = new Vector3(Hotpod.position.x,hotpoddelta,Hotpod.position.z);
        
        if(!rndmove){
            PrinterCursor.transform.position = pos;
            
        }
        
    }

    public void RndGrowPrint(float height){
          Vector3 currentpos = PrinterCursor.transform.position;
          CurrentPos = currentpos;
    
         if(!rndmove){
            RndPos = Plane.position+Random.insideUnitSphere*size*0.2f;
            Vector3 Dir = RndPos - currentpos;
            var time = Dir.magnitude/СarriageSpeed;
            MoveTime =  Time.time+time;
            step = Dir/(time/Time.deltaTime); // шаг изменения позиции
            }

            currentpos +=  step;
            currentpos =new Vector3(currentpos.x,height,currentpos.z);
            PrinterCursor.transform.position = currentpos;
            rndmove = MoveTime>=Time.time;
            SetPosition(currentpos);

    }

    IEnumerator SetOnDefaultPos(){
        Vector3 DefaultPos = new Vector3(0,9,6.28f);
        Vector3 currentpos = PrinterCursor.transform.position;
        Vector3 Dir = DefaultPos - currentpos;
            var time = Dir.magnitude/СarriageSpeed;
            MoveTime =  Time.time+time;
            step = Dir/(time/Time.fixedDeltaTime); // шаг изменения позиции
            
        while ((DefaultPos-currentpos).magnitude >=0.01f)
        {
            currentpos +=  step;
            var epos = currentpos - DefaultPos;
                if(Mathf.Abs(epos.x)<Mathf.Abs(step.x)) currentpos= new Vector3(DefaultPos.x,currentpos.y,currentpos.z);
                if(Mathf.Abs(epos.y)<Mathf.Abs(step.y)) currentpos= new Vector3(currentpos.x,DefaultPos.y,currentpos.z);
                if(Mathf.Abs(epos.z)<Mathf.Abs(step.z)) currentpos= new Vector3(currentpos.x,currentpos.y,DefaultPos.z);
            SetPosition(currentpos);
            yield return new WaitForFixedUpdate();
        }

         FindObjectOfType<GameManager>().nextBtn.SetActive(true);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, $"Complete level");
    }


    public void EndPrint(){
        GameManager.GamePhase = 6;
        StartCoroutine(SetOnDefaultPos());
    }

    bool ToggleHotpod(bool active){
        bool enabled = Hotpod.GetComponent<MeshRenderer>().enabled;
        Hotpod.GetComponent<MeshRenderer>().enabled = active;
        return enabled;
    }
    void ToggleСarriage(bool active){
        Сarriage.GetComponent<MeshRenderer>().enabled = active;
    }
    void ToggleStand(bool active){
        Stand.GetComponent<MeshRenderer>().enabled = active;
    }
    void SetPlaneRndPos(){
        var rndPart = Random.Range(0.2f,0.5f);
        rndPart = Random.value>0.5f ? -rndPart : rndPart;
        rndPart += 0.5f;
        var z = Mathf.Lerp(PlaneMoveBorder.y,PlaneMoveBorder.x,rndPart);
        Plane.transform.position = new Vector3( Plane.transform.position.x, Plane.transform.position.y,z);

    }
    public void SetCursorPosOnly(Vector3 pos){
        Сarriage.transform.position = pos+Vector3.up*1.5f;
    }

}
